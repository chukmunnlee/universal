const zoneNode = require('zone.js/dist/zone-node');
const reflectMeta = require('reflect-metadata');
const core = require('@angular/core');

//TODO: build this
//const angularServer = require('../uniang/dist/server/main');

//Express engine
const ngRenderer = require('@nguniversal/express-engine');
const moduleMap = require('@nguniversal/module-map-ngfactory-loader');

const path = require('path');
const express = require('express');

core.enableProdMode();

const app = express();

app.engine('html', ngRenderer.ngExpressEngine({
	bootstrap: angularServer.AppServerModuleFactory,
	providers: [
		moduleMap.provideModuleMap(angularServer.LAZY_MODULE_MAP)
	]
}));
app.set('view engine', 'html');
app.set('views', path.join(__dirname, '../uniang/dist/browser'))

app.get('universal', (req, resp) => {
	resp.render('index', { req: req })
})

app.use(path.join(__dirname, 'universal'));
app.use(path.join(__dirname, '../uniang/dist/browser'));

app.listen(3000, () => {
	console.log('Application started on port 3000');
});
