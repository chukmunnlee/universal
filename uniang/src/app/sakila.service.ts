import { Injectable, Inject, Optional } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { APP_BASE_HREF } from "@angular/common";

@Injectable({
	providedIn: 'root'
})
export class SakilaService {

	origin: string;

	constructor(private http: HttpClient, 
			@Optional() @Inject(APP_BASE_HREF) origin: string) { 
		this.origin = origin || '';
		console.log('Origin: ', this.origin);
	}

}