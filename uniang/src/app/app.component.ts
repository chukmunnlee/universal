import { Component, OnInit } from '@angular/core';
import { SakilaService } from './sakila.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'uniang';

  constructor(private sakilaSvc: SakilaService) { }

  ngOnInit() {
    console.log('>>>>> origin: ', this.sakilaSvc.origin);
  }
}
