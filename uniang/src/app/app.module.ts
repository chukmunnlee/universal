import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule, Inject, PLATFORM_ID, APP_ID, } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { isPlatformBrowser } from '@angular/common';

import { AppComponent } from './app.component';
import { SakilaService } from './sakila.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserTransferStateModule,
    HttpClientModule
  ],
  providers: [ ],
  bootstrap: [AppComponent]
})
export class AppModule { 

  constructor(
      @Inject(PLATFORM_ID) private platformId: Object,
      @Inject(APP_ID) private appId: string) {

    const platform = isPlatformBrowser(platformId)? "running on browser": "running on server";

    console.info('>>>> ', isPlatformBrowser(platformId))
    console.info('platformId = ', platformId)
    console.info('appId = ', appId)
  }

}
